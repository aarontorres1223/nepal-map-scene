﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class loadScene : MonoBehaviour
{
    public string scene;
    public bool IsBack;

    public void LoadVideoScene()
    {
        SceneManager.LoadScene(scene);
    }

    private void Start()
    {
        if (IsBack == false)
        {
            LoadVideoScene();
        }
        
    }

}
