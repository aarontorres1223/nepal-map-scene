﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using JsonFunction;

public class DisplayData : MonoBehaviour
{
    public TextAsset JsonFile;
    public Text Info1;
    public Text Info2;
    public Text Info3;
    public Text Info4;
    public Text Info5;
    public Text Info6;
    public Text Info7;
    public string Text1;
    public string Text2A;
    public string Text2B;
    public string Text2C;
    public string Text2D;
    public string Text2E;
    public string Text2F;
    public string Text2G;
    public int num;

    public void getDataUWSSSP()
    {
        TranslateJson.translateFromJson(JsonFile.ToString());
        Info1.text = TranslateJson.jsonDataHolder[Text1][num][Text2A].ToString();
        Info6.text = TranslateJson.jsonDataHolder[Text1][num][Text2B].ToString();
        Info2.text = TranslateJson.jsonDataHolder[Text1][num][Text2C].ToString();
        Info3.text = TranslateJson.jsonDataHolder[Text1][num][Text2D].ToString();
        Info4.text = TranslateJson.jsonDataHolder[Text1][num][Text2E].ToString();
        Info5.text = TranslateJson.jsonDataHolder[Text1][num][Text2F].ToString();
        Info7.text = TranslateJson.jsonDataHolder[Text1][num][Text2G].ToString();
    }

    public void getData3rdSTWSSSP()
    {
        TranslateJson.translateFromJson(JsonFile.ToString());
        Info1.text = TranslateJson.jsonDataHolder[Text1][num][Text2A].ToString();
        Info2.text = TranslateJson.jsonDataHolder[Text1][num][Text2B].ToString();
        Info3.text = TranslateJson.jsonDataHolder[Text1][num][Text2C].ToString();
        Info4.text = TranslateJson.jsonDataHolder[Text1][num][Text2D].ToString();
        Info5.text = TranslateJson.jsonDataHolder[Text1][num][Text2E].ToString();
        
    }

    public void getData2ndSTWSSSP()
    {
        TranslateJson.translateFromJson(JsonFile.ToString());
        Info1.text = TranslateJson.jsonDataHolder[Text1][num][Text2A].ToString();
        Info2.text = TranslateJson.jsonDataHolder[Text1][num][Text2B].ToString();
        Info3.text = TranslateJson.jsonDataHolder[Text1][num][Text2C].ToString();
        Info4.text = TranslateJson.jsonDataHolder[Text1][num][Text2D].ToString();
    }

    public void getData1stSTWSSSP()
    {
        TranslateJson.translateFromJson(JsonFile.ToString());
        Info1.text = TranslateJson.jsonDataHolder[Text1][num][Text2A].ToString();
        Info2.text = TranslateJson.jsonDataHolder[Text1][num][Text2B].ToString();
        Info3.text = TranslateJson.jsonDataHolder[Text1][num][Text2C].ToString();
        Info4.text = TranslateJson.jsonDataHolder[Text1][num][Text2D].ToString();
    }


}
