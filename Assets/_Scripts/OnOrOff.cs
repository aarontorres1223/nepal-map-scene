﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnOrOff : MonoBehaviour
{
    public bool play = false;

    public void PausePlay()
    {
        play = !play;

    }

    // Update is called once per frame
    void Update()
    {
        if (play == true)
        {
            this.gameObject.GetComponent<Animator>().enabled = false;
        }
        if (play == false)
        {
            this.gameObject.GetComponent<Animator>().enabled = true;
        }
    }
}
