﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Video;
using UnityEngine.UI;

public class OnClickPin : MonoBehaviour
{
    [SerializeField] UnityEvent anEvent;

    private void OnMouseDown()
    {
        print("You clicked the cube!");
        anEvent.Invoke();
    }

    public bool ImageAndText;
    public bool VideoAndText;
    public bool VideoAndIamge;
    public bool TextOnly;
    public bool ImageOnly;
    public bool NoContent;

    public PinContent pinContent;

	public GameObject townImage;
    public GameObject townImage2;
    public GameObject townImage3;
    public GameObject townImage4;
    public Text townInfo;
    public GameObject townVideo;
    public Image VideoPreview;
    public GameObject TLPortrait;
    public GameObject TLLandscape;


	public void ShowDetails()
    {
        App.isDetails = true;
        Debug.Log("Clicked");
        if (ImageOnly)
        {
            townImage.GetComponent<Image>().sprite = pinContent.TownImage;
            townImage2.GetComponent<Image>().sprite = pinContent.TownImage2;
            townImage3.GetComponent<Image>().sprite = pinContent.TownImage3;
            townImage4.GetComponent<Image>().sprite = pinContent.TownImage4;
            townImage2.SetActive(true);
            townImage3.SetActive(true);
            townImage4.SetActive(true);
            townImage.SetActive(true);
            townVideo.SetActive(false);
        }

        if (ImageAndText)
        {
            townImage.GetComponent<Image>().sprite = pinContent.TownImage;
            //townInfo.text = pinContent.TownInfo;
            townVideo.SetActive(false);
            townImage.SetActive(true);
        }

        if (VideoAndText)
        {
            //townVideo.GetComponent<RenderVideo>().renderTexture = pinContent.render;
            townVideo.GetComponent<VideoPlayer>().clip = pinContent.TownVideo;
            //townInfo.text = pinContent.TownInfo;
            townImage.SetActive(false);
            townVideo.SetActive(true);
        }

        if (TextOnly)
        {
            townInfo.text = pinContent.TownInfo;
            townImage.SetActive(false);
            townVideo.SetActive(false);
        }
        if (ImageAndText && VideoAndText)
        {
            townImage.GetComponent<Image>().sprite = pinContent.TownImage;
            //townInfo.text = pinContent.TownInfo;
            //townVideo.GetComponent<RenderVideo>().renderTexture = pinContent.render;
            townVideo.GetComponent<VideoPlayer>().clip = pinContent.TownVideo;
            townImage.SetActive(true);
            townVideo.SetActive(true);
        }

        if (VideoAndIamge)
        {
            townImage.GetComponent<Image>().sprite = pinContent.TownImage;
            townVideo.GetComponent<VideoPlayer>().targetTexture = pinContent.render;
            townVideo.GetComponent<VideoPlayer>().clip = pinContent.TownVideo;
            VideoPreview.sprite = pinContent.VidePreview;
            townVideo.SetActive(true);
            townImage2.GetComponent<Image>().sprite = pinContent.TownImage2;
            townImage3.GetComponent<Image>().sprite = pinContent.TownImage3;
            townImage2.SetActive(true);
            townImage3.SetActive(true);
            townImage.SetActive(true);
            townImage4.SetActive(false);
        }
        if (NoContent)
        {
            townImage.SetActive(false);
            townImage2.SetActive(false);
            townImage3.SetActive(false);
            townImage4.SetActive(false);
            townVideo.SetActive(false);
            
        }

        TLPortrait.SetActive(false);
        TLLandscape.SetActive(false);
        //Screen.orientation = ScreenOrientation.Portrait;
    }
}
