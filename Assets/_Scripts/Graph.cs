﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CodeMonkey.Utils;

public class Graph : MonoBehaviour
{
    [SerializeField] private Sprite circleSprite;

    private RectTransform container;

    private void Awake()
    {
        container = transform.Find("container").GetComponent<RectTransform>();

        //CreateCircle(new Vector2(200, 200));
        List<int> valueList = new List<int>() {1,1,2,2,2,2,2,2,3,3,3,3,3,3,3,3,3,5,5,7,7,8,8,8,9,9,10,11,11,11,12,13,14,20,23,24,29};
        ShowGraph(valueList);
    }
   
    private GameObject CreateCircle(Vector2 anchoredPos)
    {
        GameObject gameObject = new GameObject("Circle", typeof(Image));
        gameObject.transform.SetParent(container, false);
        gameObject.GetComponent<Image>().sprite = circleSprite;
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = anchoredPos;
        rectTransform.sizeDelta = new Vector2(11, 11);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);

        return gameObject;
    }

    private void ShowGraph(List<int> valueList)
    {
        float xSize = 50f; //Size distance between each point on the x axis
        float yMax = 100f; //Max value of Y;
        float graphHeight = container.sizeDelta.y;
        GameObject lastCircleGameObject = null;
        for (int i = 0; i < valueList.Count; i++)
        {
            //X
            float xPos = i * xSize + i * xSize;
            //Y
            float yPos = (valueList[i] / yMax) *graphHeight;
            GameObject circleGameobject = CreateCircle(new Vector2(xPos, yPos));
            if(lastCircleGameObject != null)
            {
                CreateDotConnection(lastCircleGameObject.GetComponent<RectTransform>().anchoredPosition, circleGameobject.GetComponent<RectTransform>().anchoredPosition); 
            }
            lastCircleGameObject = circleGameobject;
        }
    }

    private void CreateDotConnection(Vector2 dotPositionA, Vector2 dotPositionB)
    {
        GameObject gameObject = new GameObject("dotConnection", typeof(Image));
        gameObject.transform.SetParent(container, false);
        gameObject.GetComponent<Image>().color = new Color(1, 1, 1, .5f);
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        Vector2 dir = (dotPositionB - dotPositionA).normalized;
        float distance = Vector2.Distance(dotPositionA, dotPositionB);
        rectTransform.anchorMin = new Vector2(0, 0);
        rectTransform.anchorMax = new Vector2(0, 0);
        rectTransform.sizeDelta = new Vector2(distance, 3f);
        rectTransform.anchoredPosition = dotPositionA + dir * distance * .5f;
        rectTransform.localEulerAngles = new Vector3(0, 0, UtilsClass.GetAngleFromVector(dir));
    }
}
