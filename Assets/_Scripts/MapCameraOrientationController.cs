﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCameraOrientationController : MonoBehaviour
{
    public GameObject BottomControlsPortrait;
    public GameObject BottomControlsLandscape; 
    void Start()
    {
        
    }
    void Update()
    {
        if(Screen.orientation == ScreenOrientation.Landscape) {
            BottomControlsLandscape.SetActive(true);

            BottomControlsPortrait.SetActive(false);
        }

        if(Screen.orientation == ScreenOrientation.Portrait) {
            BottomControlsLandscape.SetActive(false);
            
            BottomControlsPortrait.SetActive(true);
        }
    }
}
