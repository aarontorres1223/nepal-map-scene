﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinAnimController : MonoBehaviour
{
    public Animator[] anim;
    public GameObject[] PinGroup;
    public GameObject[] Timeline;
    public float[] PinDownAnimationTiming;
    public float[] PinUpTiming;

    private void Start()
    {
        PinGroup[0].SetActive(true);
        Invoke("BlueUp", PinUpTiming[0]);
        Invoke("GreenUp", PinUpTiming[1]);
        Invoke("YellowUp", PinUpTiming[2]);
    }

    public void BlueUp()
    {
        PinGroup[1].SetActive(true);
    }
    public void GreenUp()
    {
        PinGroup[2].SetActive(true);
    }
    public void YellowUp()
    {
        PinGroup[3].SetActive(true);
        Invoke("OffAnimation", 2);
    }
    public void OffAnimation()
    {
        PinGroup[0].GetComponent<Animator>().enabled = false;
        PinGroup[1].GetComponent<Animator>().enabled = false;
        PinGroup[2].GetComponent<Animator>().enabled = false;
        PinGroup[3].GetComponent<Animator>().enabled = false;

    }

    public void OnClickYellowUp()
    {
        PinGroup[3].SetActive(true);
        PinGroup[0].SetActive(false);
        PinGroup[1].SetActive(false);
        PinGroup[2].SetActive(false);
        //anim[3].SetTrigger("up");
        //anim[0].SetTrigger("down");
        //anim[1].SetTrigger("down");
        //anim[2].SetTrigger("down");
        //Timeline[3].SetActive(true);
        //Invoke("OnClickRedDown",PinDownAnimationTiming[0]);
        //Invoke("OnClickBlueDown", PinDownAnimationTiming[1]);
        //Invoke("OnClickGreenDown", PinDownAnimationTiming[2]);
    }
    public void OnClickRedUp()
    {
        PinGroup[3].SetActive(false);
        PinGroup[0].SetActive(true);
        PinGroup[1].SetActive(false);
        PinGroup[2].SetActive(false);
        //anim[0].SetTrigger("up");
        //anim[3].SetTrigger("down");
        //anim[1].SetTrigger("down");
        //anim[2].SetTrigger("down");
        //Timeline[0].SetActive(true);
        //Invoke("OnClickYellowDown", PinDownAnimationTiming[3]);
        //Invoke("OnClickBlueDown", PinDownAnimationTiming[1]);
        //Invoke("OnClickGreenDown", PinDownAnimationTiming[2]);
    }

    public void OnClickBlueUp()
    {
        PinGroup[3].SetActive(false);
        PinGroup[0].SetActive(false);
        PinGroup[1].SetActive(true);
        PinGroup[2].SetActive(false);
        //anim[1].SetTrigger("up");
        //anim[3].SetTrigger("down");
        //anim[0].SetTrigger("down");
        //anim[2].SetTrigger("down");
        //Timeline[1].SetActive(true);
        //Invoke("OnClickYellowDown", PinDownAnimationTiming[3]);
        //Invoke("OnClickRedDown", PinDownAnimationTiming[0]);
        //Invoke("OnClickGreenDown", PinDownAnimationTiming[2]);
    }

    public void OnClickGreenUp()
    {
        PinGroup[3].SetActive(false);
        PinGroup[0].SetActive(false);
        PinGroup[1].SetActive(false);
        PinGroup[2].SetActive(true);
        //anim[2].SetTrigger("up");
        //anim[0].SetTrigger("down");
        //anim[1].SetTrigger("down");
        //anim[3].SetTrigger("down");
        //Timeline[2].SetActive(true);
        //Invoke("OnClickYellowDown", PinDownAnimationTiming[3]);
        //Invoke("OnClickRedDown", PinDownAnimationTiming[0]);
        //Invoke("OnClickBlueDown", PinDownAnimationTiming[1]);
    }

    public void OnClickAllUp()
    {
        PinGroup[0].SetActive(true);
        PinGroup[1].SetActive(true);
        PinGroup[2].SetActive(true);
        PinGroup[3].SetActive(true);
        //anim[2].SetTrigger("up");
        //anim[0].SetTrigger("up");
        //anim[1].SetTrigger("up");
        //anim[3].SetTrigger("up");
    }

    public void OnClickYellowDown()
    {
        PinGroup[3].SetActive(false);
        Timeline[3].SetActive(false);
    }

    public void OnClickRedDown()
    {
        PinGroup[0].SetActive(false);
        Timeline[0].SetActive(false);
    }

    public void OnClickBlueDown()
    {
        PinGroup[1].SetActive(false);
        Timeline[1].SetActive(false);
    }

    public void OnClickGreenDown()
    {
        PinGroup[2].SetActive(false);
        Timeline[2].SetActive(false);
    }

    
}
