﻿using System.Collections;
using LitJson;
using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Text;

namespace JsonFunction
{
    public class GetJsonFromServer
    {


        public static string JsonRawText;
        public static IEnumerator GetJsonTextFromLink(string ServerLink)
        {
            //gets the api from server
            UnityWebRequest Server = UnityWebRequest.Get(ServerLink);
            yield return Server.SendWebRequest();

            if (Server.isNetworkError || Server.isHttpError)
            {
                Debug.Log(Server.error);
            }
            else
            {
                //gets the raw json text so it can be called in another method
                JsonRawText = Server.downloadHandler.text;
                translateFromJson(JsonRawText);


            }
        }

        public static JsonData jsonDataHolder;
        public static void translateFromJson(string jsonRaw)
        {

            jsonDataHolder = JsonMapper.ToObject(jsonRaw);
            return;
        }

    }

    public class GetMuralJsonFromServer
    {


        public static string JsonRawText;
        public static IEnumerator GetJsonTextFromLink(string ServerLink)
        {
            //gets the api from server
            UnityWebRequest Server = UnityWebRequest.Get(ServerLink);
            yield return Server.SendWebRequest();

            if (Server.isNetworkError || Server.isHttpError)
            {
                Debug.Log(Server.error);
            }
            else
            {
                //gets the raw json text so it can be called in another method
                JsonRawText = Server.downloadHandler.text;
                translateFromJson(JsonRawText);


            }
        }

        public static JsonData jsonDataHolder;
        public static void translateFromJson(string jsonRaw)
        {

            jsonDataHolder = JsonMapper.ToObject(jsonRaw);
            return;
        }

    }

    public class GetExplorerJsonFromServer
    {


        public static string JsonRawText;
        public static IEnumerator GetJsonTextFromLink(string ServerLink)
        {
            //gets the api from server
            UnityWebRequest Server = UnityWebRequest.Get(ServerLink);
            yield return Server.SendWebRequest();

            if (Server.isNetworkError || Server.isHttpError)
            {
                Debug.Log(Server.error);
            }
            else
            {
                //gets the raw json text so it can be called in another method
                JsonRawText = Server.downloadHandler.text;
                translateFromJson(JsonRawText);


            }
        }

        public static JsonData jsonDataHolder;
        public static void translateFromJson(string jsonRaw)
        {

            jsonDataHolder = JsonMapper.ToObject(jsonRaw);
            return;
        }

    }

    public class GetHaranaJsonFromServer
    {


        public static string JsonRawText;
        public static IEnumerator GetJsonTextFromLink(string ServerLink)
        {
            //gets the api from server
            UnityWebRequest Server = UnityWebRequest.Get(ServerLink);
            yield return Server.SendWebRequest();

            if (Server.isNetworkError || Server.isHttpError)
            {
                Debug.Log(Server.error);
            }
            else
            {
                //gets the raw json text so it can be called in another method
                JsonRawText = Server.downloadHandler.text;
                translateFromJson(JsonRawText);


            }
        }

        public static JsonData jsonDataHolder;
        public static void translateFromJson(string jsonRaw)
        {

            jsonDataHolder = JsonMapper.ToObject(jsonRaw);
            return;
        }

    }

    public class TranslateJson
    {
        //translates json to c#
        public static JsonData jsonDataHolder;
        public static void translateFromJson(string JsonText)
        {
            jsonDataHolder = JsonMapper.ToObject(JsonText);
            return;
        }
    }
    //[Serializable]
    public class JsonPost
    {

        public static IEnumerator UploadJson(string url, string param)
        {
            //post 
            Debug.Log(param);
            using (UnityWebRequest PostData = UnityWebRequest.Put(url, param))
            {
                byte[] bytes = GetBytes(param);
                UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
                PostData.uploadHandler = uH;
                uH.contentType = "application/json";
                PostData.method = UnityWebRequest.kHttpVerbPOST;
                yield return PostData.SendWebRequest();

                if (PostData.isNetworkError || PostData.isHttpError)
                {
                    Debug.Log(PostData.error);
                }
                else
                {
                    Debug.Log("Upload complete!");
                }
            }

        }
        //convert string to byte[]
        static byte[] GetBytes(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            return bytes;
        }

    }

    public class JsonPut
    {

        public static IEnumerator PutJson(string url, string param)
        {
            //post 
            Debug.Log(param);
            using (UnityWebRequest PutData = UnityWebRequest.Put(url, param))
            {
                byte[] bytes = GetBytes(param);
                UploadHandlerRaw uH = new UploadHandlerRaw(bytes);
                PutData.uploadHandler = uH;
                uH.contentType = "application/json";
                PutData.method = UnityWebRequest.kHttpVerbPUT;
                yield return PutData.SendWebRequest();

                if (PutData.isNetworkError || PutData.isHttpError)
                {
                    Debug.Log(PutData.error);
                }
                else
                {
                    Debug.Log("Upload complete!");
                }
            }

        }
        //convert string to byte[]
        static byte[] GetBytes(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);
            return bytes;
        }

    }

}