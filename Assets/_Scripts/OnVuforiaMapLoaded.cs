﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnVuforiaMapLoaded : MonoBehaviour
{
    private GameObject MainScene;
    private GameObject Camera;
    private GameObject V;

    private GameObject F;
    private GameObject T;
    private GameObject TB;

    /*private void OnEnable()
    {
        MainScene.SetActive(false);
    }

    private void OnDisable()
    {
        MainScene.SetActive(true);
        gameObject.SetActive(false);
    }*/

    private void Start()
    {
        MainScene = GameObject.Find("Tutorial button");
        Camera = GameObject.Find("CameraParent");
        MainScene.SetActive(false);
        Camera.SetActive(false);
    }

    public void OnBackClick()
    {

        //V = GameObject.Find("Vuforia Map");
        MainScene.SetActive(true);
        Camera.SetActive(true);
        //F = GameObject.Find("Front Page");
        //F.SetActive(false);
        //T = GameObject.Find("Tutorial");
        //T.SetActive(false);
        //TB = GameObject.Find("Tutorial Button");
        //TB.SetActive(true);
        Destroy(gameObject);
    }
}
