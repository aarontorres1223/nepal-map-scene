﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class ReverseNotificationController : MonoBehaviour
{
    [Header ("3D Models")]
    public GameObject[] Models;
    [Header ("Text Files")]
    //public string[] paths;
    public TextAsset[] TextFiles;
    [Header ("Notification GameObject")]
    public GameObject Notification;
    [Header ("Description Text")]
    public Text txtDesc;
    [Header ("Buttons")]
    public GameObject btnPrev;
    public GameObject btnNext;
    public GameObject btnUp;
    public GameObject btnDown;
    public GameObject btnReset;
    [Header ("Parent Gameobject of Items")]
    public GameObject parentOfTheModels;
    [SerializeField]
    private int index = 0;
    //vince
    public int PublicIndex;

    private void Start()
    {
        index = PublicIndex;
        //Set the First Item of the Array as the Default
        App.ModelName = Models[App.currentIndex].name;
        index = App.currentIndex;
        btnPrev.GetComponent<Button>().interactable = false;
        for (int i = 0; i <= Models.Length; i++)
        {
            if (Models[i].name == App.ModelName)
            {
                Models[i].SetActive(true);
                GetDescription();
            }
            else
            {
                Models[i].SetActive(false);
            }
        }
    }

    private void OnEnable()
    {
        
        //vince
        index = 0;
        App.currentIndex = 0;
        //Set the First Item of the Array as the Default
        App.ModelName = Models[App.currentIndex].name;
        index = App.currentIndex;
        btnPrev.GetComponent<Button>().interactable = false;
        //vince
        btnNext.GetComponent<Button>().interactable = true;
        for (int i = 0; i <= Models.Length; i++)
        {
            if (Models[i].name == App.ModelName)
            {
                Models[i].SetActive(true);
                GetDescription();
            }
            else
            {
                Models[i].SetActive(false);
            }
        }
    }

    private void OnDisable()
    {
        
        //vince
        for (int i = 0; i <= Models.Length; i++)
        {
            Models[i].SetActive(false);   
        }
        //vince
        index = 0;
        App.currentIndex = 0;
        //Set the First Item of the Array as the Default
        App.ModelName = Models[App.currentIndex].name;
        index = App.currentIndex;
        btnPrev.GetComponent<Button>().interactable = false;
        for (int i = 0; i <= Models.Length; i++)
        {
            if (Models[i].name == App.ModelName)
            {
                Models[i].SetActive(true);
                GetDescription();
            }
            else
            {
                Models[i].SetActive(false);
            }
        }
    }

    public void OnClickNext()
    {
        int ml = Models.Length - 1;
        index++;
        if (index >= 0 && index <= ml)
        {
            App.ModelName = Models[index].name;
            App.currentIndex = index;

            //Debug.Log("Index " + index);
            //Debug.Log("Models Lenght " + Models.Length);

            //This is the Water Treatment Object
            //Last Index of the Array
            if(index == ml)
            {
                btnNext.GetComponent<Button>().interactable = false;

                btnUp.SetActive(true);
                btnDown.SetActive(true);
                btnReset.SetActive(true);
            }

            for (int i = 0; i <= ml; i++)
            {
                if (Models[i].name == App.ModelName)
                {

                    Models[i].SetActive(true);
                    btnPrev.GetComponent<Button>().interactable = true;
            
                    //Always put this on the last
                    //TODO: Fix the Array out of Bound Error
                    GetDescription();
                    
                }
                else
                {
                    Models[i].SetActive(false);
                }
            }
        }
    }

    public void OnClickPrev()
    {
        HideAll();
        //Make sure there is no negative index
        //or 0
        if (index < 0)
        {
            index = 0;
            App.currentIndex = index;
           
        } else
        {
            if(index != 0 && index > 0)
            {
                index = App.currentIndex - 1;
            } else
            {
                index = 0;
                App.currentIndex = index;
            } 
        }

        /*if (index == 0)
        {
            Models[index].SetActive(true);
            GetDescription();
            btnPrev.GetComponent<Button>().interactable = false;
            btnNext.GetComponent<Button>().interactable = true;
        }*/
       
        if (index >= 0 && index <= Models.Length)
        {
            App.ModelName = Models[index].name;
            App.currentIndex = index;

            for (int i = 0; i <= Models.Length; i++)
            {
                if (Models[i].name == App.ModelName)
                {

                    Models[i].SetActive(true);
                    btnNext.GetComponent<Button>().interactable = true;
                    //TODO: Fix the Array out of Bound Error
                    GetDescription();
                }
                else
                {
                    Models[i].SetActive(false);
                }
            }
        }
    }

    //Hide All Gameobject
    void HideAll()
    {
        for (int i = 0; i < Models.Length; i++)
        {
            Models[i].SetActive(false);
        }

        //Disable the Water Treatment Button Controls
        btnUp.SetActive(false);
        btnDown.SetActive(false);
        btnReset.SetActive(false);
    }

    //Read from a Text Assets 
    void ReadTextFromFile(string path)
    {
        StreamReader reader = new StreamReader(path);
        txtDesc.text = reader.ReadToEnd();
        txtDesc.enabled = true;
    
        reader.Close();
        Notification.SetActive(true);
    }

    //Get the Description from the Text Assets
    public void GetDescription()
    {
        /*for (int i = 0; i <= Models.Length; i++)
        {
            if (Models[i].name == App.ModelName)
            {
                ReadTextFromFile(paths[i]);
            }
        }*/
        for (int i = 0; i < parentOfTheModels.transform.childCount; i++)
        {
            if(parentOfTheModels.transform.GetChild(i).gameObject.activeSelf == true)
            {
                if(parentOfTheModels.transform.GetChild(i).name == App.ModelName)
                {
                    string name = parentOfTheModels.transform.GetChild(i).name;
                    for (int x = 0; x <= TextFiles.Length; x++)
                    {
                        if(name == TextFiles[x].name)
                        {
                            //ReadTextFromFile(@"Assets/_Scripts/TextAssets/" + name + ".txt");
                            //ReadTextFromFile(Path.Combine(Application.streamingAssetsPath + "/TextAssets/" + name + ".txt"));

                            //Ariez
                            //For Android Only
                            string _path = "TextAssets/" + name;
                            TextAsset sourceFile = (TextAsset)Resources.Load(_path, typeof(TextAsset));
                            txtDesc.text = sourceFile.text;

                            txtDesc.enabled = true;
                            Notification.SetActive(true);
                        }
                    }
                }
            }
        }
    }
}
