﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
[CreateAssetMenu(fileName = "Pin Content", menuName = "Pin Content")]
public class PinContent : ScriptableObject
{
    public string TownInfo;
    public Sprite TownImage;
    public Sprite TownImage2;
    public Sprite TownImage3;
    public Sprite TownImage4;
    public Sprite VidePreview;
    public VideoClip TownVideo;
    public RenderTexture render;
}
