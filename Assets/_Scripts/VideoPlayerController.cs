﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using UnityEngine.Events;
using VideoHandling;

public class VideoPlayerController : MonoBehaviour , IDragHandler , IPointerDownHandler
{
    [SerializeField]
    VideoPlayer videoPlayer;
    [SerializeField]
    Text statusText;

    Image progressbar;

    public bool isVuforia;
    public bool isPlaying;
    bool isFirstTime = true;

    public bool isVideoController;
    public bool isProgressbar;
    public bool isASeparateScene;

    public GameObject BackButton;


    [SerializeField] UnityEvent anEvent;

    private void OnMouseDown()
    {
        print("You clicked the cube!");
        anEvent.Invoke();
    }

    public void OnDrag(PointerEventData eventData)
    {
        Tryskip(eventData);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Tryskip(eventData);
    }

    void Tryskip(PointerEventData eventData)
    {
        Vector2 localpoint;
        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(
            progressbar.rectTransform,eventData.position,null,out localpoint))
        {
            float pct = Mathf.InverseLerp(progressbar.rectTransform.rect.xMin, progressbar.rectTransform.rect.xMax, localpoint.x);
            SkipToPercent(pct);
        }
    }

    private void SkipToPercent(float pct)
    {
        var frame = videoPlayer.frameCount * pct;
        videoPlayer.frame = (long)frame;
    }

    private void Awake()
    {
        progressbar = GetComponent<Image>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (isVuforia) {
            isPlaying = true;
        } else {
            //isPlaying = false;
        }

        if (isASeparateScene)
        {
            videoPlayer.frame = VideoHandling.VideoProgress.CurrentVideoFrame;
            //Debug.Log("" + VideoHandling.VideoProgress.CurrentVideoFrame);
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        LastVideoFrame();
        if (isVideoController)
        {
            if(isPlaying == true)
            {
                PlayVideo();
            }
            if (isPlaying == false)
            {
                PauseVideo();
            }

        }

        if (isProgressbar)
        {
            if (videoPlayer.frameCount > 0)
            {
                progressbar.fillAmount = (float)videoPlayer.frame / (float)videoPlayer.frameCount;
            }
        }
        
    }

    public void Switch()
    {
        isPlaying = !isPlaying;
    }

    public void PauseVideo()
    {
        videoPlayer.Pause();   
        statusText.text = "PAUSE";
        BackButton.SetActive(true);
        //StartCoroutine(statusTextWaitTime(3f));
        //Debug.Log("video paused");
    }

    public void PlayVideo()
    {
        videoPlayer.Play();
        statusText.text = "";
        BackButton.SetActive(false);
        //Debug.Log("video played");
    }

    public void ReplayVideo()
    {
        videoPlayer.frame = 0;
    }

    public void LastVideoFrame()
    {
        VideoHandling.VideoProgress.CurrentVideoFrame = videoPlayer.frame;
        //Debug.Log("aaaaaaaaaaa"+videoPlayer.frame);
    }

    IEnumerator statusTextWaitTime(float time) {
        yield return new WaitForSeconds(time);
        statusText.enabled = false;
    }

}
