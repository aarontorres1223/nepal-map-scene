﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

#if UNITY_EDITOR
using UnityEditor;
#endif

[System.Serializable]
public class VideoCollector {  //Made for Video Collection to be sent at Video Panel Scene

    public string VideoName;
    public VideoClip video;
    [HideInInspector] public bool hasScript; //trying to make custom inspector
    public TextAsset Subtitle;

}

public class VideoHandler : MonoBehaviour {

    private static GameObject instance;

    public VideoCollector[] videoCollector;
    public int VideoNumber;

    private void Start() {
        DontDestroyOnLoad(transform.gameObject);
        if (instance == null)
            instance = gameObject;
        else
            Destroy(gameObject);
    }
}
/*
#if UNITY_EDITOR
[CustomEditor(typeof(VideoHandler))]
public class RandomScript_Editor : Editor {
    public override void OnInspectorGUI() {
        DrawDefaultInspector(); // for other non-HideInInspector fields

        VideoHandler script = (VideoHandler)target;

        script.videoCollector[0].hasScript = EditorGUILayout.Toggle("Has Script", script.videoCollector[0].hasScript);
        if (script.videoCollector[0].hasScript) // if bool is true, show other fields
        {
            script.videoCollector[0].Subtitle = EditorGUILayout.ObjectField("Subtitle", script.videoCollector[0].Subtitle, typeof(TextAsset), true) as TextAsset;

        }

              // draw checkbox for the bool
        
    }
}
#endif

*/