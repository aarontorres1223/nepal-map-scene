﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Used for having a reference for the video Handler for each of the buttons
/// </summary>

public class ButtonReferenceHandler : MonoBehaviour {

    private GameObject videoRef;
    public int VideoNumber;

    void Start() {
        videoRef = GameObject.Find("VideoHandler");
    }

    //Send video array number to place data for VideoHandler Script
    public void SendVideoData(int videoNumber) { 
        videoRef.GetComponent<VideoHandler>().VideoNumber = videoNumber;

    }
}
