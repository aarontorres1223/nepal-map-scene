﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PhoneFunction : App {

    private static GameObject instance;
    private int Action;
    private Scene PrevScene;
    private Scene CurrentScene;
    private string sceneName;
    private AutoRotate autoRotate;

    void Start() {
        autoRotate = GetComponent<AutoRotate>();
        DontDestroyOnLoad(transform.gameObject);
        if (instance == null)
            instance = gameObject;
        else
            Destroy(gameObject);
    }

    private void Update() {
        SceneManager.sceneLoaded += OnSceneLoaded;

        if (Application.platform == RuntimePlatform.Android ||
            Application.platform == RuntimePlatform.WindowsEditor) {

            if (Input.GetKeyUp(KeyCode.Escape)) {

                if (sceneName == "Main Menu") {
                    //Action = 0;
                    ExitApp();
                    return;
                } else if (sceneName == "Panels") {
                    // Action = 1;
                    LoadAScene("Main Menu");
                } else if (sceneName == "VideoPanel" || sceneName == "360Video") {
                    // Action = 2;
                    autoRotate.OnVideoExit();
                    LoadAScene("Panels");
                } 
            } 
            /*
            switch (Action)
            {
                case 0: //At Main Menu and pressed back to quit
                    ExitApp();
                    Action = default;
                    break;

                case 1: // At Panels and want to go back to main menu
                    LoadAScene("Main Menu");
                    Action = default;
                    break;

                case 2:
                    LoadAScene("Panels");
                    Action = default;
                    break;

                default:
                    break;
            }

            */
        }
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        CurrentScene = SceneManager.GetActiveScene();
        sceneName = CurrentScene.name;
    }

}
