﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/// <summary>
/// All Functions related to map handling \
/// Has Zoom Functions
/// Has Swipe Functions 
/// Has Slider for move options
/// </summary>
public class MapAspects : MonoBehaviour {

	//Zoom Functions
	Camera mainCamera;
	float touchesPrevPosDifference, touchesCurPosDifference, zoomModifier;
	Vector2 firstTouchPrevPos, secondTouchPrevPos;
	[SerializeField]
	float zoomModifierSpeed = 0.1f;

	[Header("Swipe Settings")]  //Swipe
	bool canSwipe;
	[SerializeField]
	float maxSwipeTime;

	[SerializeField]
	GameObject ObjectToRotate;

	// Used for checking 
	[SerializeField]
	Canvas canvas;
	GraphicRaycaster m_Raycaster;
	PointerEventData m_PointerEventData;
	EventSystem m_EventSystem;

	// Use this for initialization
	void Start() {
		mainCamera = GetComponent<Camera>();
		m_Raycaster = canvas.GetComponent<GraphicRaycaster>();
	}

	// Update is called once per frame
	void Update() {

        if (canSwipe) {
			SwipeFunctions();
		}

		if (Input.GetButtonDown("Fire1")) {
			
			//Set up the new Pointer Event
			m_PointerEventData = new PointerEventData(m_EventSystem);
			//Set the Pointer Event Position to that of the first finger position
			m_PointerEventData.position = Input.GetTouch(0).position;

			//Create a list of Raycast Results
			List<RaycastResult> results = new List<RaycastResult>();

			m_Raycaster.Raycast(m_PointerEventData, results);

			//For every result returned, output the name of the GameObject on the Canvas hit by the Ray
			foreach (RaycastResult result in results) {
				Debug.Log("Hit " + result.gameObject.name);

				if (result.gameObject.name == "TouchArea") {
					canSwipe = true;
					
				} else {
					canSwipe = false;
				}
			}
		}

		ZoomFunctions();
		
	}

	void ZoomFunctions() {
		if (Input.touchCount == 2) {
			Touch firstTouch = Input.GetTouch(0);
			Touch secondTouch = Input.GetTouch(1);

			firstTouchPrevPos = firstTouch.position - firstTouch.deltaPosition;
			secondTouchPrevPos = secondTouch.position - secondTouch.deltaPosition;

			touchesPrevPosDifference = (firstTouchPrevPos - secondTouchPrevPos).magnitude;
			touchesCurPosDifference = (firstTouch.position - secondTouch.position).magnitude;

			zoomModifier = (firstTouch.deltaPosition - secondTouch.deltaPosition).magnitude * zoomModifierSpeed;

			if (touchesPrevPosDifference > touchesCurPosDifference)
				mainCamera.fieldOfView += zoomModifier;
			if (touchesPrevPosDifference < touchesCurPosDifference)
				mainCamera.fieldOfView -= zoomModifier;

		}

		mainCamera.fieldOfView = Mathf.Clamp(mainCamera.fieldOfView, 4f, 60f);
	}

	void SwipeFunctions() {
		if (Input.touchCount > 0) {
			var touch = Input.touches[0];

			if (touch.phase == TouchPhase.Moved && Input.touchCount == 1) {
				ObjectToRotate.transform.RotateAround(ObjectToRotate.transform.position, -ObjectToRotate.transform.up * -touch.deltaPosition.x , Time.deltaTime * 200f);

			}
		}
	}
}
