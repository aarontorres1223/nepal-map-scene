﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SliderMove : MonoBehaviour {

    public GameObject objectToMove;
    public Slider slider;
    public bool canMove;
    public Button button;

    // Preserve the original and current orientation
    private float previousValue;

    void Awake() {
        // Assign a callback for when this slider changes
        slider.onValueChanged.AddListener(OnSliderChanged);

        // And current value
        previousValue = slider.value;
    }

    void OnSliderChanged(float value) {
        float delta = value - this.previousValue;
        //objectToMove.transform.position = new Vector3(slider.value, objectToMove.transform.position.y, objectToMove.transform.position.z);
        //objectToMove.transform.RotateAround(objectToMove.transform.position, objectToMove.transform.forward, value);
        objectToMove.transform.Rotate(Vector3.forward * delta * 360);
        //objectToMove.transform.rotation = Quaternion.Euler(0 , 0, delta * 360);
        // Set our previous value for the next change
        previousValue = value;

    }
}
