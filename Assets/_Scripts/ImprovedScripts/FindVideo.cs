﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

/// <summary>
/// Holds all the videos to be sent at the Video Panel Scene
/// Keeps it inside an array with the name and Video with it.
/// </summary>

public class FindVideo : MonoBehaviour {

    [SerializeField] private GameObject VideoRef;             // Reference for Object "VideoHandler"
    private int refNum;                                       //Array referenceNumber passed from Panel Scene
    public GameObject Subtitle;

    //Gets the video Handler and asks for the clip needed to play
    private void Start() {
        VideoRef = GameObject.Find("VideoHandler");
        Subtitle = GameObject.Find("Subtitle");
        refNum = VideoRef.GetComponent<VideoHandler>().VideoNumber;
        PlayVideo();
        checkSubs();
    }

    void PlayVideo() {
        gameObject.GetComponent<VideoPlayer>().clip = VideoRef.GetComponent<VideoHandler>().videoCollector[refNum].video;
    }

    //Checks if the Video Handler has subs
    void checkSubs() {
        
        if(VideoRef.GetComponent<VideoHandler>().videoCollector[refNum].Subtitle != null) {
            Subtitle.GetComponent<SubtitleDisplayer>().Subtitle =  VideoRef.GetComponent<VideoHandler>().videoCollector[refNum].Subtitle;
        }
    }

    public void ClearRefNum() {
        refNum = 0;
    }
}

