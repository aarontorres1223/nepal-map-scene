﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoTutorial : MonoBehaviour
{
    public GameObject[] videoTutorial;
    public GameObject tutorialOnButtonL;
    public GameObject tutorialOffButtonL;
    public GameObject tutorialOnButtonP;
    public GameObject tutorialOffButtonP;
    public GameObject PortraitControls;
    public VideoPlayer vp;
    public GameObject vpComponent;

    string screenOrientation = "";
    bool isLandscape = false;
    bool isPortrait = false;

    // Start is called before the first frame update

    void Start() {
        //PlayerPrefs.SetInt("IsFirst", 1);
        if (PlayerPrefs.GetInt("IsFirst") == 0) {
            vpComponent.GetComponent<VideoPlayerController>().isPlaying = false;
            vp.Pause();
            videoTutorial[0].SetActive(true);

           if(screenOrientation == "landscape") {
                //Disable All Controls while on landscape
                tutorialOnButtonP.SetActive(false);
                tutorialOffButtonP.SetActive(false);
            } else if(screenOrientation == "portrait"){
                tutorialOnButtonP.SetActive(false);
                tutorialOffButtonP.SetActive(true);
            }
        } 
        
        if(PlayerPrefs.GetInt("IsFirst") == 1) {
            vpComponent.GetComponent<VideoPlayerController>().isPlaying = true;
            vp.Play();

            for (int y = 0; y <= videoTutorial.Length; y++) {
                videoTutorial[y].SetActive(false);
            }

            if (screenOrientation == "landscape") {
                //Disable All Controls while on landscape
                tutorialOnButtonP.SetActive(false);
                tutorialOffButtonP.SetActive(false);
            } else if (screenOrientation == "portrait") {
                tutorialOnButtonP.SetActive(false);
                tutorialOffButtonP.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    void Update() {
        
        if (Screen.orientation == ScreenOrientation.LandscapeRight || Screen.orientation == ScreenOrientation.LandscapeLeft) {
            screenOrientation = "landscape";

            //Disable All Controls while on landscape
            PortraitControls.SetActive(false);
        } else if(Screen.orientation == ScreenOrientation.Portrait) {
            screenOrientation = "portrait";

            //Enable All Controls on Portrait
            //PortraitControls.SetActive(true); //temp disable
        }

        //Debug.Log(screenOrientation);
    }

    public void TutorialOnClick() {
        vpComponent.GetComponent<VideoPlayerController>().isPlaying = false;
        if (screenOrientation == "landscape") {
            //Disable All Controls while on landscape
            tutorialOnButtonP.SetActive(false);
            tutorialOffButtonP.SetActive(false);

            videoTutorial[0].SetActive(true);
        } else if (screenOrientation == "portrait") {
            tutorialOffButtonP.SetActive(true);
            tutorialOnButtonP.SetActive(false);

            videoTutorial[0].SetActive(true);
        }

        PlayerPrefs.SetString("Tutorial", "true");
        PlayerPrefs.Save();
    }
    public void TutorialOffClick() {
        vpComponent.GetComponent<VideoPlayerController>().isPlaying = true;
        if (screenOrientation == "landscape") {
            //Disable All Controls while on landscape
            tutorialOnButtonP.SetActive(false);
            tutorialOffButtonP.SetActive(false);
        } else if (screenOrientation == "portrait") {
            tutorialOffButtonP.SetActive(false);
            tutorialOnButtonP.SetActive(true);
        }

        for (int y = 0; y <= videoTutorial.Length; y++) {
            videoTutorial[y].SetActive(false);
        }

        PlayerPrefs.SetString("Tutorial", "false");
        PlayerPrefs.Save();
    }

    public void onOkVideoTutorial(int index) {
        //vpComponent.GetComponent<VideoPlayerController>().isPlaying = true;
        videoTutorial[index + 1].SetActive(true);
        if (index > 0) {
            videoTutorial[index - 1].SetActive(false);
        } else {
            videoTutorial[0].SetActive(false);
        }
    }

    public void onStartVideo() {
        vpComponent.GetComponent<VideoPlayerController>().isPlaying = true;
        vp.Play();
        TutorialOffClick();
    }
}
