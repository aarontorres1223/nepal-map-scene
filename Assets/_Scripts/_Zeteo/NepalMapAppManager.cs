﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NepalMapAppManager : MonoBehaviour
{
    public GameObject NepalMap;
    public GameObject NepalPrefab;
    //public Camera camera;
    public GameObject ZoomObj;


    public void Start() {

    }
    public void rotateLeft() {
        NepalMap.transform.RotateAround(NepalMap.transform.position, NepalMap.transform.up, Time.deltaTime * 1000f);
    }

    public void rotateRight() {
        NepalMap.transform.RotateAround(NepalMap.transform.position, -NepalMap.transform.up, Time.deltaTime * 1000f);
    }

    public void zoomIn() {
        ZoomObj.GetComponent<OrthoZoomAndPan>().Zoom(.1f);
        //NepalPrefab.GetComponent<MouseObjControl>().zoom -= 5;
        //camera.fieldOfView++;
    }

    public void zoomOut() {
        ZoomObj.GetComponent<OrthoZoomAndPan>().Zoom(-.1f);
        //NepalPrefab.GetComponent<MouseObjControl>().zoom += 5;
    }
}
