﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class App : MonoBehaviour
{
    //Author: Ariez Flores
    //Root Script for Zeteo App

    //Globals
    public static string AppVersion;
    public static string ModelName;
    public static int currentIndex = 0;
    public static int mFirst = 0;
    public static bool _tapStatus = false;
    public static bool isAnimDone = false;
    public static bool isOutputs = false;
    public static bool isPlaying = false;
    public static bool isReset = false;
    public static bool isDetails = false;

    //tutorial bools
    public static bool ScanTutorial;
    public static bool FirstVidTutorial;
    public static bool SecondVidTutorial;
    public static bool OutputsTutorial;
    public static bool TreatmentPlantTutorial;

    //Inspector
   [HideInInspector] public Text buildText;

    private void Start()
    {
        //GetAppVersion();
    }

    public  void GetAppVersion()
    {
        AppVersion = Application.version;
        //buildText.text = AppVersion;
    }

    public void LoadAScene(string scene)
    {
        SceneManager.LoadScene(scene);

    }

    public void ExitApp() {
        Application.Quit();
    }

   public void AutoRotation() {
        Screen.orientation = ScreenOrientation.AutoRotation;
   }
}
