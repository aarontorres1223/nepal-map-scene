﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NepalMapOrientationController : MonoBehaviour
{
    public GameObject TimelinePortrait;
    public GameObject TimelineLandscape;
    public GameObject BottomPanelPortrait;
    public GameObject BottomButtonsPortrait;
    public GameObject BottomPanelLandscape;
    public GameObject BottomButtonsLandscape;
    public GameObject Out;
    public GameObject In;

    void Update()
    {
        if(Screen.orientation == ScreenOrientation.LandscapeRight || Screen.orientation == ScreenOrientation.LandscapeLeft) {
            if (App.isReset) {
                //TimelineLandscape.SetActive(false);
                //TimelinePortrait.SetActive(false);
            } else {
                //TimelineLandscape.SetActive(true);
                BottomPanelLandscape.SetActive(true);
                BottomButtonsLandscape.SetActive(true);

                //TimelineLandscape.transform.position = In.transform.position;
                //TimelinePortrait.transform.position = Out.transform.position;

                //TimelinePortrait.SetActive(false);
                BottomPanelPortrait.SetActive(false);
                BottomButtonsPortrait.SetActive(false);
            }

            if (App.isDetails) {
                //TimelineLandscape.SetActive(false);
                //TimelinePortrait.SetActive(false);
            }
           
        }

        if(Screen.orientation == ScreenOrientation.Portrait) {
            if (App.isReset) {
                //TimelineLandscape.SetActive(false);
                //TimelinePortrait.SetActive(false);
            } else {
                //TimelineLandscape.SetActive(false);
                BottomPanelLandscape.SetActive(false);
                BottomButtonsLandscape.SetActive(false);

                //TimelineLandscape.transform.position = Out.transform.position;
                //TimelinePortrait.transform.position = In.transform.position;

                //TimelinePortrait.SetActive(true);
                BottomPanelPortrait.SetActive(true);
                BottomButtonsPortrait.SetActive(true);
            }

            if (App.isDetails) {
                //TimelineLandscape.SetActive(false);
                //TimelinePortrait.SetActive(false);
            }

        }
    }

}
