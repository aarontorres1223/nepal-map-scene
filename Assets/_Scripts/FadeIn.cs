﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeIn : MonoBehaviour
{
    public GameObject[] Blinders;
    public GameObject Prefab;

    public void OnClickDestroy()
    {
        Destroy(Prefab);
    }

    public void Play1()
    {
        Blinders[0].SetActive(true);
        Blinders[1].SetActive(false);
        Blinders[2].SetActive(false);

    }

    public void Play2()
    {
        Blinders[1].SetActive(true);
        Blinders[0].SetActive(false);
        Blinders[2].SetActive(false);

    }

    public void Play3()
    {
        Blinders[2].SetActive(true);
        Blinders[0].SetActive(false);
        Blinders[1].SetActive(false);

    }

    [SerializeField] UnityEvent anEvent;

    private void OnMouseDown()
    {
        print("You clicked the cube!");
        anEvent.Invoke(); 
    }

    public void EventClick()
    {
        print("Which also triggered this method as a UnityEvent!");
    }


}
