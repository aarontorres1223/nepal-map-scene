﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderUI : MonoBehaviour
{
    public RenderTexture renderTexture;

    // Start is called before the first frame update
    void Start()
    {
        renderTexture = new RenderTexture(1920, 1080, 0, RenderTextureFormat.RGB565);
        renderTexture.useMipMap = false;
        renderTexture.filterMode = FilterMode.Point;
        renderTexture.anisoLevel = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnGUI()
    {
        Rect r = new Rect(10, 70, 50, 30);
        GUI.DrawTexture(r, renderTexture);
        if (Input.GetMouseButton(0))
        {
            if (r.Contains(Event.current.mousePosition))
            {

            }
        }
    }
}
