﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lookAtCamera : MonoBehaviour
{
    public GameObject lookAtThis;

    // Update is called once per frame
    void Update()
    {
        transform.LookAt(lookAtThis.transform);
    }
}
