﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotate : MonoBehaviour
{
    //Calling this will enable scree auto rotation for videos
    public void OnVideoEnter() { 
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;
        Screen.autorotateToPortrait = true;
        Screen.orientation = ScreenOrientation.AutoRotation;
    }

    //Calling this will disable screen rotation and go back to portait
    public void OnVideoExit() {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        Screen.autorotateToPortrait = false;
        Screen.orientation = ScreenOrientation.Portrait;
    }

    public void On360Video() {
        Screen.autorotateToPortrait = false;
        Screen.orientation = ScreenOrientation.Landscape;
    }
}
