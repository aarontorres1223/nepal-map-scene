﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager : MonoBehaviour
{
    public GameObject Panel1;
    public GameObject Panel2;

    public void fromStartExperience()
    {
        Panel1.SetActive(false);
        Panel2.SetActive(true);
    }

    public void fromTutorial()
    {
        Panel1.SetActive(true);
        Panel2.SetActive(false);
    }
}
