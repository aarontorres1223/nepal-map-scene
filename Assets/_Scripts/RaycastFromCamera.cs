﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastFromCamera : MonoBehaviour
{
    public Camera camera;
    public GameObject TapAnim;
    public GameObject SmartPhoneAnim;
    public Text text;

    private void Update()
    {
        RaycastHit hit;
        Ray ray = camera.ScreenPointToRay(camera.transform.position);

        if(Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;

            StartCoroutine(delay(3f));
;        }
    }

    IEnumerator delay(float waitTime)
    {
        yield return new WaitForSeconds(waitTime);
        text.text = "Plane is Ready..";
        SmartPhoneAnim.SetActive(false);

        if (!App._tapStatus)
        {
            TapAnim.SetActive(true);
        } else
        {
            TapAnim.SetActive(false);
        }
        
    }
}
