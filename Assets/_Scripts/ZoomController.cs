﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZoomController : MonoBehaviour
{
    public Button ZoomButton;

    public int CurrentVideoPlaying;

    public GameObject[] Planes;

    public void Start()
    {
        CurrentVideoPlaying = 1;
    }

    public void BackButton()
    {
        Planes[0].GetComponent<Animator>().SetTrigger("stay");
        Planes[1].GetComponent<Animator>().SetTrigger("stay");
        Planes[2].GetComponent<Animator>().SetTrigger("stay");
    }

    public void ZoomVid()
    {
        if (CurrentVideoPlaying == 1)
        {
            Planes[0].GetComponent<Animator>().SetTrigger("Zoom1");
            Planes[1].GetComponent<Animator>().SetTrigger("Move2");
            Planes[2].GetComponent<Animator>().SetTrigger("Move3");
        }
        if (CurrentVideoPlaying == 2)
        {
            Planes[1].GetComponent<Animator>().SetTrigger("Zoom2");
            Planes[0].GetComponent<Animator>().SetTrigger("Move1");
            Planes[2].GetComponent<Animator>().SetTrigger("Move3");
        }
        if (CurrentVideoPlaying == 3)
        {
            Planes[2].GetComponent<Animator>().SetTrigger("Zoom3");
            Planes[1].GetComponent<Animator>().SetTrigger("Move2");
            Planes[0].GetComponent<Animator>().SetTrigger("Move1");
        }

    }

    

    public void Playing1()
    {
        CurrentVideoPlaying = 1;
    }

    public void Playing2()
    {
        CurrentVideoPlaying = 2;
    }

    public void Playing3()
    {
        CurrentVideoPlaying = 3;
    }

    private void Update()
    {
        
    }

}
