﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.IO;

public class WaterFacilityScript : MonoBehaviour
{
    public Text DescriptionTextHolder;
    public TextAsset Description;
    [SerializeField] UnityEvent anEvent;

    private void OnMouseDown()
    {
        print("You clicked the cube!");
        anEvent.Invoke();
    }

    public void ChangeDescription()
    {
        //StreamReader reader = new StreamReader(Description.ToString());

        //DescriptionTextHolder.text = reader.ReadToEnd().ToString();
        DescriptionTextHolder.text = Description.ToString();
    }
}
