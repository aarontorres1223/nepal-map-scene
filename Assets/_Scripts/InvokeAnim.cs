﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvokeAnim : MonoBehaviour
{
    //Author: Ariez Flores
    //This Script will invoke the animation at x seconds


    [Header ("Animation Clip")]
	public AnimationClip animClip;
    [Header("Invoke Wait Time")]
    public float waitTime;
    [Header("Animation Controller")]
    public RuntimeAnimatorController rAC;
    private Animator anim;
    

    private void Start()
    {
        anim = GetComponent<Animator>();
        InvokeRepeating("PlayAnimation", 2.0f, waitTime);
    }

    void PlayAnimation()
    {
        StartCoroutine(removeandadd());
    }

    IEnumerator removeandadd()
    {
        anim.runtimeAnimatorController = null;
        yield return new WaitForSeconds(.1f);
        anim.runtimeAnimatorController = rAC;
    }
}
