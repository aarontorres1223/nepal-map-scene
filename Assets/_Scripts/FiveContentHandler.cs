﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FiveContentHandler : MonoBehaviour
{
    public GameObject ContentHandler;
    public GameObject PrevButton;
    public GameObject NextButton;
    public GameObject WTFTutorial;
    public GameObject[] Models;
    bool WTFTutorialOn = true;
    int ScreenCounter;

    public void Next()
    {
        //ContentHandler.transform.Translate(Vector3.left * (Time.deltaTime + 15.5f));
        ScreenCounter++;
    }

    public void Prev()
    {
        //ContentHandler.transsform.Translate(Vector3.right * (Time.deltaTime+15.5f));
        ScreenCounter--;
    }
    // Start is called before the first frame update
    void Start()
    {
        ScreenCounter = 0;
        Models[0].SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        if (ScreenCounter <= 0)
        {
            PrevButton.GetComponent<Button>().interactable = false;
            App.ModelName = "Faucet Only";
            Models[0].SetActive(true);
            Models[1].SetActive(false);
            Models[2].SetActive(false);
            Models[3].SetActive(false);
        }

        if (ScreenCounter <= 1)
        {
            App.ModelName = "Meter Reading";
            Models[0].SetActive(false);
            Models[1].SetActive(true);
            Models[2].SetActive(false);
            Models[3].SetActive(false);
        }

        if (ScreenCounter <= 2)
        {
            App.ModelName = "Seminar";
            Models[0].SetActive(false);
            Models[1].SetActive(false);
            Models[2].SetActive(true);
            Models[3].SetActive(false);
        }

        if (ScreenCounter >= 3)
        {
            NextButton.GetComponent<Button>().interactable = false;
            App.ModelName = "Water Facilities";
            for (int i = 0; i < Models.Length; i++)
            Models[0].SetActive(false);
            Models[1].SetActive(false);
            Models[2].SetActive(false);
            Models[3].SetActive(true);
        }

        else
        {
            NextButton.GetComponent<Button>().interactable = true;
            PrevButton.GetComponent<Button>().interactable = true;
        }

        if (WTFTutorialOn)
        {
            if (ScreenCounter >= 3)
            {
                WTFTutorial.SetActive(true);

            }
        }
        
    }

    public void WTFTutorialOff()
    {
        WTFTutorialOn = false;
    }
}
