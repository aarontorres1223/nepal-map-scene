﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class OnClickFullScreenImage : MonoBehaviour
{
    public GameObject ImageHolder;
    public GameObject FullScreenImag;
    public GameObject FullScreenVideo;

    

    public void GoFullscreen()
    {
        FullScreenImag.SetActive(true);
        FullScreenImag.gameObject.GetComponent<Image>().sprite = ImageHolder.GetComponent<Image>().sprite;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    public void GoFullScreenVideo()
    {
        FullScreenVideo.SetActive(true);
        FullScreenVideo.GetComponent<VideoPlayer>().clip = ImageHolder.GetComponent<VideoPlayer>().clip;
        FullScreenVideo.GetComponent<VideoPlayer>().targetTexture = ImageHolder.GetComponent<VideoPlayer>().targetTexture;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }
}
