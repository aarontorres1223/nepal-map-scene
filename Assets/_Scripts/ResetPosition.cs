﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPosition : MonoBehaviour
{
    public Transform OriginalPos;
    public GameObject Parent;
    public GameObject child;
    public Quaternion quat;

    // Start is called before the first frame update
    void Start() {
        
        OriginalPos.rotation = OriginalPos.transform.rotation;
        OriginalPos.position = gameObject.transform.position;
        OriginalPos.localScale = gameObject.transform.localScale;
        quat = gameObject.transform.rotation;

        print(OriginalPos.rotation.z + ":Z");
    }

    public void ResetOBJ()
    {
        this.gameObject.transform.rotation = OriginalPos.rotation;
        this.gameObject.transform.position = OriginalPos.position;
        this.gameObject.transform.localScale = OriginalPos.localScale;
        Parent.gameObject.transform.rotation = OriginalPos.rotation;
        Parent.gameObject.transform.position = OriginalPos.position;
        Parent.gameObject.transform.localScale = OriginalPos.localScale;
        child.gameObject.transform.rotation = OriginalPos.rotation;
        child.gameObject.transform.position = OriginalPos.position;
        child.gameObject.transform.localScale = OriginalPos.localScale;
    }

    public void rotateUp()
    {
        quat.x += .1f;
        this.gameObject.transform.rotation = quat;
    }

    public void rotateDown()
    {
        quat.x -= .1f;
        this.gameObject.transform.rotation = quat;
    }


}
