﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayItOnce : MonoBehaviour
{
    public Animator animator;
    public bool isEnable = false;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(App.isAnimDone);
        if(App.isAnimDone == true){
            animator.enabled = false;
            isEnable = false;
        }else if(App.isAnimDone == false){
            animator.enabled = true;
           isEnable = true;
            /*bool status = animator.GetCurrentAnimatorStateInfo(0).length > animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            Debug.Log("Status: " + status);
            if(status){
                App.isAnimDone = true;
            }else{
                App.isAnimDone = false;
            }*/
        }
    }
    

    // Update is called once per frame
    void Update()
    {
        /*if(isEnable){
            animator.enabled = true;
        }*/
        //Debug.Log(App.isAnimDone);
        if(App.isAnimDone == true){
            animator.enabled = false;
            isEnable = false;
        }else if(App.isAnimDone == false){
            animator.enabled = true;
           isEnable = true;
            
            bool status = animator.GetCurrentAnimatorStateInfo(0).length > animator.GetCurrentAnimatorStateInfo(0).normalizedTime;
            Debug.Log("Status: " + status);
            if(status){
                App.isAnimDone = false;
            }else{
                App.isAnimDone = true;
            }
        }
    }

    void OnEnabled(){
        
    }
}
