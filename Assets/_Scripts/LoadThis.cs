﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadThis : MonoBehaviour
{
    GameObject VuforiaObject;

    // Start is called before the first frame update
    void Start()
    {
        if (VuforiaObject = GameObject.Find("Vuforia Parent"))
        {
            VuforiaObject.transform.GetChild(0).gameObject.SetActive(true);
        }

        //VuforiaObject.SetActive(true)
        StartCoroutine(selfDestruct(2f));
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator selfDestruct(float Time)
    {
        yield return new WaitForSeconds(Time);
        Destroy(gameObject);
        //Debug.Log("Destroy");
    }
}
