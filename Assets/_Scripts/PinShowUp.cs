﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PinShowUp : MonoBehaviour
{
    public float TimeStart;
    public float TimeEnd;
    public float speed;
    [SerializeField]
    bool isOn;

    public void MovePinUp()
    {
        float step = speed * Time.deltaTime;
        //float step = speed;
        transform.position = Vector3.MoveTowards(transform.position,transform.position + new Vector3(0,1,0), step);
    }

    public void SteadyPin()
    {
        transform.position = Vector3.MoveTowards(transform.position, transform.position + new Vector3(0, 0, 0), 0);
    }

    public void OnAnim()
    {
        isOn = true;
        Invoke("OffAnim", TimeEnd);
    }

    public void OffAnim()
    {
        isOn = false;
    }

    // Start is called before the first frame update
    void Update()
    {
        Invoke("OnAnim", TimeStart);

        if (isOn)
        {
            MovePinUp();
        }
        if (!isOn)
        {
            this.GetComponent<PinShowUp>().enabled = false;
        }
    }

   


}
