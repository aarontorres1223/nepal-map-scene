﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    //public GameObject Map;
    //public GameObject MainMenu;
    //public GameObject ARSessionGO;
    public string scene;
    public bool isSC;

    private void Start()
    {
        if (isSC)
        {

        }
        else
        {
            Invoke("LoadMainMenu", 3);
        }
        
    }

    public void LoadMainMenu()
    {
        //SceneManager.LoadScene(scene);
        StartCoroutine(wait(scene));
        StartCoroutine(loadLevelAsync(scene));
    }

    public void LoadMapScene()
    {
        //SceneManager.LoadScene(10);
        StartCoroutine(wait("Map Scene"));
        StartCoroutine(loadLevelAsync("Map Scene"));
        //StartCoroutine(waitForMapLoad());
    }

    IEnumerator waitForMapLoad() {
        yield return new WaitForSeconds(3f);
        //ARSessionGO.SetActive(true);
        //Map.SetActive(true);
        //MainMenu.SetActive(false);
    }

    //Enable the loading animation
    IEnumerator wait(string level) {
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(level);
    }

    //Load scene in the background
    IEnumerator loadLevelAsync(string level) {
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(level);

        while (!asyncOperation.isDone) {
            yield return null;
        }
    }
}
