﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetOutput : MonoBehaviour
{
    public GameObject Outputs;

    // Start is called before the first frame update
    void Start()
    {
        Outputs.GetComponent<ReverseNotificationController>().PublicIndex = 0;
        App.currentIndex = 0;
    }

    
}
