﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PhotoCarouselController : MonoBehaviour
{
    [SerializeField] UnityEvent anEvent;

    public bool[] onoff;
    public int[] a;
    public GameObject Text;

    private void Start()
    {
        a[0] = 0;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        a[5] = 0;
        a[6] = 0;
        a[7] = 0;
    }

    private void OnMouseDown()
    {
        print("You clicked the cube!");
        anEvent.Invoke();
    }

    public void playAnim(int x)
    {
        onoff[x] = !onoff[x];
        
    }

    private void Update()
    {
        if (onoff[0] == true && a[0] == 0)
        {
            Zoom1();
        }

        if (onoff[0] == false && a[0] == 1)
        {
            Out1();
        }
        if (onoff[1] == true && a[1] == 0)
        {
            Zoom2();
        }

        if (onoff[1] == false && a[1] == 1)
        {
            Out2();
        }
        if (onoff[2] == true && a[2] == 0)
        {
            Zoom3();
        }

        if (onoff[2] == false && a[2] == 1)
        {
            Out3();
        }
        if (onoff[3] == true && a[3] == 0)
        {
            Zoom4();
        }

        if (onoff[3] == false && a[3] == 1)
        {
            Out4();
        }
        if (onoff[4] == true && a[4] == 0)
        {
            Zoom5();
        }

        if (onoff[4] == false && a[4] == 1)
        {
            Out5();
        }
        if (onoff[5] == true && a[5] == 0)
        {
            Zoom6();
        }

        if (onoff[5] == false && a[5] == 1)
        {
            Out6();
        }
        if (onoff[6] == true && a[6] == 0)
        {
            Zoom7();
        }

        if (onoff[6] == false && a[6] == 1)
        {
            Out7();
        }
    }

    public void Zoom1()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 1");
        a[0] = 1;
    }

    public void Zoom2()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 2");
        a[1] = 1;
    }

    public void Zoom3()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 3");
        a[2] = 1;
    }

    public void Zoom4()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 4");
        a[3] = 1;
    }

    public void Zoom5()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 5");
        a[4] = 1;
    }

    public void Zoom6()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 6");
        a[5] = 1;
    }

    public void Zoom7()
    {
        Text.SetActive(true);
        gameObject.GetComponent<Animator>().SetTrigger("zoom 7");
        a[6] = 1;
    }

    public void Out1()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 1");
        a[0] = 0;
    }

    public void Out2()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 2");
        a[1] = 0;
    }

    public void Out3()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 3");
        a[2] = 0;
    }

    public void Out4()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 4");
        a[3] = 0;
    }

    public void Out5()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 5");
        a[4] = 0;
    }

    public void Out6()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 6");
        a[5] = 0;
    }

    public void Out7()
    {
        Text.SetActive(false);
        gameObject.GetComponent<Animator>().SetTrigger("out 7");
        a[6] = 0;
    }
}
