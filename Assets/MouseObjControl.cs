﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseObjControl : MonoBehaviour {

    public Camera camera;

    //MouseMovement
    [SerializeField] float rotateSpeed;
    bool canDrag;
    Rigidbody rb;

    //Scroll Zoom
    public float zoom;
    [SerializeField] float zoomChangeAmount;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update() {
        Zoom();

        if (Input.GetMouseButtonUp(0)) {
            canDrag = false;
        }

        if (canDrag) {
            print("YEET");
            float x = Input.GetAxis("Mouse X") * rotateSpeed * Time.deltaTime;
            float y = Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;

            rb.AddTorque(Vector3.down * x);
            rb.AddTorque(Vector3.right * y);
           
        }
    }

    private void OnMouseDrag() {
        canDrag = true;
    }

    private void Zoom() {
        if (Input.mouseScrollDelta.y > 0) {
            zoom -= zoomChangeAmount * Time.deltaTime * 10f;
        }

        if (Input.mouseScrollDelta.y < 0) {
            zoom += zoomChangeAmount * Time.deltaTime * 10f;

        }


        zoom = Mathf.Clamp(zoom, 10f, 100f);
        camera.fieldOfView = zoom;
    }
}
